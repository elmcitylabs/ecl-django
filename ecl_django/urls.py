from .utils import template_url

urlpatterns = [
    template_url("auth/password/reset/sent"),
]
