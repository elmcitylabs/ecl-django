#!/usr/bin/env python
# -*- coding: utf-8 -*-

# Copyright 2012-2019 Elm City Labs LLC
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

import runpy

try:
    from setuptools import setup  # type: ignore
except ImportError:
    from distutils.core import setup

metadata_filename = "ecl_django/metadata.py"
metadata = runpy.run_path(metadata_filename)

setup(
    name="ecl_django",
    version=metadata["__version__"],
    license=metadata["__license__"],
    url="https://bitbucket.org/elmcitylabs/ecl-django/src/master/",
    description="Django decorators and some other utilities.",
    author="Dan Loewenherz",
    author_email="dan@elmcitylabs.com",
    packages=["ecl_django", "ecl_django.management", "ecl_django.management.commands"],
    install_requires=["django>=3.0", "py-bcrypt", "ecl-tools>=1.0.1"],
)

