#!/bin/bash

git add .
git ci -m "bump version to $1"
git archive --format=tar master > ecl_django-$1.tar
gzip -f ecl_django-$1.tar
s3cmd put ecl_django-$1.tar.gz s3://packages.elmcitylabs.com/ -P
